'use strict'; // Strict Mode

$( document ).ready(function() {
	GIS.layers.Measure = new L.FeatureGroup();

	//change default tooltip
	L.drawLocal.draw.toolbar.undo.text = 'Удалить последнюю точку';
	L.drawLocal.draw.toolbar.actions.text = 'Отмена';

	L.drawLocal.draw.toolbar.buttons.polyline = 'Измерить расстояние';
	L.drawLocal.draw.toolbar.buttons.polygon = 'Измерить площадь';

	L.drawLocal.draw.handlers.polyline.tooltip.start = 'Кликните, чтобы начать рисовать линию.';
	L.drawLocal.draw.handlers.polyline.tooltip.cont = 'Кликните, чтобы продолжить рисовать линию.';
	L.drawLocal.draw.handlers.polyline.tooltip.end = 'Двойной клик завершит линию.';

	L.drawLocal.draw.handlers.polygon.tooltip.start = 'Кликните, чтобы начать рисовать полигон.';
	L.drawLocal.draw.handlers.polygon.tooltip.cont = 'Кликните, чтобы продолжить рисовать полигон.';
	L.drawLocal.draw.handlers.polygon.tooltip.end = 'Двойной клик завершит полигон.';

	L.GeometryUtil.readableDistance = function (distance) {
		return SizeLine(distance);
	}

	L.GeometryUtil.readableArea = function(area) {
		return SizeArea(area);
	};


	GIS.controls.measure = new L.Control.Draw({
		position: 'topright',
		draw: {
			position: 'topright',
			
			polygon: {
				allowIntersection: true,
				drawError: {
					color: '#2757EE',
					timeout: 1000
				},
				shapeOptions: {
					color: '#2757EE'
				},
				showArea: true,
			},
			polyline: {
				shapeOptions: {
					stroke: true,
					color: '#2757EE',
					weight: 4,
					opacity: 0.7,
					fill: false,
					clickable: true
				},
				metric: true,
				showLength: true
			},
			circle: false,
			marker: false,
			rectangle: false,
		},
		edit: {
			featureGroup: GIS.layers.Measure,
			edit: false,
			remove: false,
		}
	});

	var SizeLine = function( l ) { 
		var km = Math.floor(l / 1000);
		var m = Math.ceil(l - km * 1000);
		
		return (km > 0 ? km + ' км ' : '') + (m > 0 ? m + ' м ' : '');
	}

	var SizeArea = function( l ) { 
		var km = Math.floor(l / 10000);
		var m = Math.ceil(l - km * 10000);
		
		return (km > 0 ? km + ' км&sup2 ' : '') + (m > 0 ? m + ' м&sup2 ' : '');
	}

	//Measure distance
	var MeasureLine = function(layer) {
		var latlngs = layer._latlngs; 
		var distance = 0;

		for (var i = 0; i <= latlngs.length - 2; i++) {
			distance +=  latlngs[i].distanceTo(latlngs[i + 1]);
		}
		
		layer.bindPopup( '<span class = "text" style = "font-size: 13px" >' + 'Длинна: ' + SizeLine(distance) + '</span>' );
	}

	//Measure area - get from draw plugin
	var MeasureArea = function(layer) {

		var latlngs = layer._latlngs; 
		var area = 0.0;

		if (latlngs.length > 2) {
			for (var i = 0; i < latlngs.length; i++) {
				var p1 = latlngs[i];
				var p2 = latlngs[(i + 1) % latlngs.length];
				area += ( (p2.lng - p1.lng) * L.LatLng.DEG_TO_RAD ) * ( 2 + Math.sin(p1.lat * L.LatLng.DEG_TO_RAD) + Math.sin(p2.lat * L.LatLng.DEG_TO_RAD) );
			}
			area *= 6378137.0 * 6378137.0 / 2.0;
		}
		
		layer.bindPopup( '<span class = "text" style = "font-size: 13px" >' + 'Площадь: ' + SizeArea( Math.abs(area) ) + '</span>' );
	}

	GIS.map.on('draw:created', function (e) {
		var layer = e.layer;
		GIS.layers.Measure.addLayer(layer);
	});

	GIS.map.on('draw:drawstop', function (e) {
		var type = e.layerType;
		var layer = '';
		
		for (var i in GIS.layers.Measure._layers) {
			layer = GIS.layers.Measure._layers[i];
		}
		
		if (type == 'polyline') {
			MeasureLine(layer);
		} else if (type == 'polygon') {
			MeasureArea(layer);
		}
		
		layer.openPopup();
	});

	GIS.map.addLayer(GIS.layers.Measure);
	GIS.map.addControl(GIS.controls.measure);

	var measure_container = document.getElementsByClassName('leaflet-draw-toolbar leaflet-bar leaflet-draw-toolbar-top')[0];
	var measure_remove_control = L.DomUtil.create('a', 'leaflet-draw-draw-remove', measure_container);
	measure_remove_control.title = 'Удалить измерения';
	measure_remove_control.onclick = function() { GIS.layers.Measure.clearLayers() };

});
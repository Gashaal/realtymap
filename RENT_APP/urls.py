from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib import admin
admin.autodiscover()
import views as core

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'RENT_APP.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', core.Core),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^login/', core.login),
    url(r'^create_account/', core.create_account),
)

urlpatterns.extend( 
	patterns('',
		url(r'^' + 'MAP' + '/', include('MAP' + '.urls')),
        url(r'^' + 'SIDEBAR' + '/', include('SIDEBAR' + '.urls')),
	)
)
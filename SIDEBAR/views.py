# -*- coding utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, render_to_response
from django.http import HttpResponse
import json
from CORE import models, config
	
def get_search_params(request):
	
	response = {}
	response['objects_types'] = {}

	response['objects_types']['flat'] = {}
	response['objects_types']['flat']['types'] = models.FLAT_TYPE_CHOICES
	response['objects_types']['flat']['prices'] = config.PRICES['rent']['flat']
	
	response['objects_types']['dacha'] = {}
	response['objects_types']['dacha']['types'] = models.DACHA_TYPE_CHOICES
	response['objects_types']['dacha']['prices'] = config.PRICES['rent']['dacha']

	response['objects_types']['list'] = config.OBJECTS_TYPE_LIST

	return HttpResponse(json.dumps(response), mimetype='application/json')
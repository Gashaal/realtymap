'use strict'; // Strict Mode

$( document ).ready(function() {

	var urls = {
		getParams: GIS.ENV.STATIC_GIS + 'SIDEBAR/get_search_params',
	}

	var Search = Backbone.Model.extend({

		initialize: function() {
			var urls = this.get('urls');
			var model = this;

			$.get(
				urls.getParams,
				function(data) {
					if (data.objects_types.list) {
						console.log(data.objects_types.list);
						model.setOptions('search-type', data.objects_types.list);
						$('#search-type').prepend($("<option selected></option>").attr("value", 'all').text('Все виды недвижимости'));
						

						$('.nstSlider').nstSlider({
						    "left_grip_selector": ".leftGrip",
						    "right_grip_selector": ".rightGrip",
						    "value_bar_selector": ".bar",
						    "value_changed_callback": function(cause, leftValue, rightValue, prevLeft, prevRight) {
						        var $grip = $(this).find('.leftGrip'),
						            whichGrip = 'left grip';
						        if (leftValue === prevLeft) {
						            $grip = $(this).find('.rightGrip');
						            whichGrip = 'right grip';
						        }
						        $('#area-start').text(leftValue);
						        $('#area-finish').text(rightValue);
						     
						    }
						});

					};
				}
			)
		},

		setOptions: function(elementId, options) {
			var element = $('#' + elementId).empty();

			_.each(options, function(key, value) {
				console.log(key);
				element.append($("<option></option>").attr("value", value).text(key))
			});
		},
	})

	GIS.search = new Search({urls: urls});

})
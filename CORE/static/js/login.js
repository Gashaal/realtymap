'use strict'; // Strict Mode

$( document ).ready(function() {

	GIS.login = function() {
		var user = $('#login-user')[0];
		var password = $('#login-password')[0];

		$.post(
		  'login/',
		  {user: user.value, password: password.value},
		  function(data) {
		  		var login_error = $('#login-error')[0];

			  	if (data.fullname) {
			  		$('#login-dialog').modal('hide');
			  		login_error.style.display = 'none';
			  		user.value = '';
			  		password.value = '';

			  		var nav_user_entered = $('#nav-user-entered')[0];
			  		nav_user_entered.style.display = 'block';

			  		var user_name_text = $('#user-name-text')[0];
			  		user_name_text.textContent = 'Вы вошли как ' + data.fullname;

			  		var nav_user_not_entered = $('#nav-user-not-entered')[0];
			  		nav_user_not_entered.style.display = 'none';
			  	};

			  	if (data == 'error') {
			  		login_error.style.display = 'block'; 
			  	};
		  }
		);
	}

	GIS.create_account = function() {
		var email = $('#create-email')[0]; 
		var user = $('#create-user')[0];
		var password = $('#create-password')[0];

		$.post(
		  'create_account/',
		  {email: email.value, username: user.value, password: password.value},
		  function(data) {
		  		var login_error = $('#create-error')[0];

			  	if (data == 'ok') {
			  		$('#login-dialog').modal('hide');
			  		login_error.style.display = 'none';
			  		user.value = '';
			  		password.value = '';

			  	};

			  	if (data == 'error') {
			  		login_error.style.display = 'block'; 
			  	};
		  }
		);
	}

});

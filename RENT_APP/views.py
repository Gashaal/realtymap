from django.shortcuts import render_to_response
from django.http import HttpResponse
from django.template import RequestContext, Context
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from settings import STATIC_URL, STATIC_GIS
import json

def Core(request):
	c = {}
	c['STATIC_URL'] = STATIC_URL
	c['STATIC_GIS'] = STATIC_GIS

	return render_to_response('core.html', c)

@csrf_exempt
def login(request):
	username = request.POST['user']
	password = request.POST['password']
	user = authenticate(username=username, password=password)
	if user is not None:
		if user.is_active:
			result_json = {}
			if user.first_name <> '' and user.last_name <> '':
				result_json['fullname'] = user.last_name + ' ' + user.first_name
			else:
				result_json['fullname'] = user.username
				
			return HttpResponse(json.dumps(result_json), mimetype='json/application')

		else:
			return HttpResponse('disabled account')
	else:
		return HttpResponse('error')

@csrf_exempt
def create_account(request):
	
	email = request.POST['email']
	username = request.POST['username']
	password = request.POST['password']

	try:
		user = User.objects.create_user(username, email, password)	
		user.save()
		return HttpResponse('ok')
	except Exception, e:
		return HttpResponse('error')
	
	
from django.shortcuts import render, render_to_response
from CORE import models
from django.http import HttpResponse
import json
from django.template import RequestContext

# Create your views here.

def geojson(request):
	from django.contrib.gis.geos import Polygon

	geojson = {
		'type': 'FeatureCollection',
		'features': [],
		'crs': {
			'type': 'name',
			'properties': {
				'name': 'urn:ogc:def:crs:OGC:1.3:CRS84'
			}
		}
	}

	FeatureClass = request.GET['FeatureClass']
	FeatureField = request.GET['FeatureField'].split(',')
	FeatureQuery = request.GET['FeatureQuery']
	bbox = request.GET['BBOX'].split(',')
	for b in bbox:
		b = float(b)

	bbox = tuple(bbox)
	poly = Polygon.from_bbox(bbox)
	
	layer = models.__dict__[FeatureClass].objects.filter(geom__bboverlaps=poly).values()

	for l in layer:
		properties = {}
		for f in FeatureField:
			properties[f.strip()] = l[f.strip()]

		geometry = json.loads(l['geom'].geojson)
		geojson['features'].append({
			'type': 'Feature',
			'geometry': {
				'type': geometry['type'],
				'coordinates': geometry['coordinates']
			},
			'properties': properties
		})

	return HttpResponse(json.dumps(geojson), mimetype='application/json')


def get_popup(request):
	
	model = request.GET['model']
	guid = request.GET['guid']

	result = {}

	if model == 'flat':
		result['data'] = get_flat_popup(guid)
		template = 'flat.html'
	if model == 'dacha':
		result['data'] = get_dacha_popup(guid)
		template = 'dacha.html'
	if model == 'plot':
		result['data'] = get_plot_popup(guid)
		template = 'plot.html'
	if model == 'office':
		result['data'] = get_office_popup(guid)
		template = 'office.html'
	if model == 'trade':
		result['data'] = get_trade_popup(guid)
		template = 'trade.html'
	if model == 'warehouse':
		result['data'] = get_warehouse_popup(guid)
		template = 'warehouse.html'
	if model == 'production':
		result['data'] = get_production_popup(guid)
		template = 'production.html'
	if model == 'garage':
		result['data'] = get_garage_popup(guid)
		template = 'garage.html'


	return render_to_response(template, result, context_instance=RequestContext(request))

def get_flat_popup(guid):

	try:
		flat = models.flat.objects.get(guid=guid)

		data = {}
		
		data['guid'] = flat.guid
		data['photo'] = flat.photo
		data['price'] = flat.price
		data['date'] = flat.date
		data['type_object'] = flat.get_type_object_display()
		data['street'] = flat.street
		data['house'] = flat.house
		data['fraction'] = flat.fraction
		data['total_area'] = flat.total_area
		data['living_area'] = flat.living_area
		data['kitchen_area'] = flat.kitchen_area
		data['floor_flat'] = flat.floor_flat
		data['floors'] = flat.floors

		return data

	except Exception, e:
		return 'deleted'


def get_dacha_popup(guid):

	try:
		dacha = models.dacha.objects.get(guid=guid)

		data = {}

		data['guid'] = dacha.guid
		data['photo'] = dacha.photo
		data['price'] = dacha.price
		data['date'] = dacha.date
		data['type_object'] = dacha.get_type_object_display()
		data['street'] = dacha.street
		data['house'] = dacha.house
		data['fraction'] = dacha.fraction
		data['total_area'] = dacha.total_area
		data['living_area'] = dacha.living_area
		data['kitchen_area'] = dacha.kitchen_area
		data['plot_area'] = dacha.kitchen_area

		return data

	except Exception, e:
		return 'deleted'

	
def get_plot_popup(guid):

	try:
		plot = models.plot.objects.get(guid=guid)

		data = {}

		data['guid'] = plot.guid
		data['photo'] = plot.photo
		data['price'] = plot.price
		data['date'] = plot.date
		data['type_object'] = plot.get_type_object_display()
		data['street'] = plot.street
		data['house'] = plot.house
		data['fraction'] = plot.fraction
		data['plot_area'] = plot.plot_area

		return data

	except Exception, e:
		return 'deleted'


def get_office_popup(guid):

	try:
		office = models.office.objects.get(guid=guid)

		data = {}

		data['guid'] = office.guid
		data['photo'] = office.photo
		data['price'] = office.price
		data['date'] = office.date
		data['type_object'] = office.get_type_object_display()
		data['street'] = office.street
		data['house'] = office.house
		data['fraction'] = office.fraction
		data['room_area'] = office.room_area
		data['plot_area'] = office.plot_area

		return data

	except Exception, e:
		return 'deleted'


def get_trade_popup(guid):

	try:
		trade = models.trade.objects.get(guid=guid)

		data = {}

		data['guid'] = trade.guid
		data['photo'] = trade.photo
		data['price'] = trade.price
		data['date'] = trade.date
		data['type_object'] = trade.get_type_object_display()
		data['street'] = trade.street
		data['house'] = trade.house
		data['fraction'] = trade.fraction
		data['room_area'] = trade.room_area
		data['plot_area'] = trade.plot_area

		return data

	except Exception, e:
		return 'deleted'


def get_warehouse_popup(guid):

	try:
		warehouse = models.warehouse.objects.get(guid=guid)

		data = {}

		data['guid'] = warehouse.guid
		data['photo'] = warehouse.photo
		data['price'] = warehouse.price
		data['date'] = warehouse.date
		data['type_object'] = warehouse.get_type_object_display()
		data['street'] = warehouse.street
		data['house'] = warehouse.house
		data['fraction'] = warehouse.fraction
		data['room_area'] = warehouse.room_area
		data['plot_area'] = warehouse.plot_area

		return data

	except Exception, e:
		return 'deleted'


def get_warehouse_popup(guid):

	try:
		warehouse = models.warehouse.objects.get(guid=guid)

		data = {}

		data['guid'] = warehouse.guid
		data['photo'] = warehouse.photo
		data['price'] = warehouse.price
		data['date'] = warehouse.date
		data['type_object'] = warehouse.get_type_object_display()
		data['street'] = warehouse.street
		data['house'] = warehouse.house
		data['fraction'] = warehouse.fraction
		data['room_area'] = warehouse.room_area
		data['plot_area'] = warehouse.plot_area

		return data

	except Exception, e:
		return 'deleted'


def get_production_popup(guid):

	try:
		production = models.production.objects.get(guid=guid)

		data = {}

		data['guid'] = production.guid
		data['photo'] = production.photo
		data['price'] = production.price
		data['date'] = production.date
		data['type_object'] = production.get_type_object_display()
		data['street'] = production.street
		data['house'] = production.house
		data['fraction'] = production.fraction
		data['room_area'] = production.room_area
		data['plot_area'] = production.plot_area

		return data

	except Exception, e:
		return 'deleted'


def get_garage_popup(guid):

	try:
		garage = models.garage.objects.get(guid=guid)

		data = {}

		data['guid'] = garage.guid
		data['photo'] = garage.photo
		data['price'] = garage.price
		data['date'] = garage.date
		data['type_object'] = garage.get_type_object_display()
		data['street'] = garage.street
		data['house'] = garage.house
		data['fraction'] = garage.fraction
		data['area'] = garage.area

		return data

	except Exception, e:
		return 'deleted'
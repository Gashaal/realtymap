'use strict'; // Strict Mode

$( document ).ready(function() {
	
    GIS.map = L.map('map', {maxZoom: 18, zoomControl: false}).setView([47.246377, 39.70905], 13);

    //define tiles
	if (GIS.layers.OSM == undefined) {
		GIS.layers.OSM = {}
	}

	GIS.layers.OSM.OpenStreetMap = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
		attribution: '',
	});
	GIS.layers.OSM.OpenStreetMap.addTo(GIS.map);

	/*GIS.layers.DGIS = new L.DGis();

	GIS.layers.Google_road = new L.Google('ROADMAP');
	GIS.layers.Google_satellite = new L.Google('SATELLITE');

	GIS.layers.Yandex_road = new L.Yandex('map');
	GIS.layers.Yandex_satellite = new L.Yandex('satellite');

	var baseLayers = {
		'2ГИС': GIS.layers.DGIS,
	    "OpenStreetMap": GIS.layers.OSM.OpenStreetMap,
	    'Google карта': GIS.layers.Google_road,
	    'Google спутник': GIS.layers.Google_satellite,
	    'Yandex карта': GIS.layers.Yandex_road,
	    'Yandex спутник': GIS.layers.Yandex_satellite,
    };*/

	//define controls

	GIS.controls.sidebar = L.control.sidebar('sidebar', {
	    position: 'left',
	    closeButton: false
	});

	GIS.map.addControl(GIS.controls.sidebar);
	GIS.controls.sidebar.show();

	//this control mast be added on the map first
	var leaflet_container = document.getElementsByClassName('leaflet-sidebar')[0];
	var sidebar_div = L.DomUtil.create('div', 'leaflet-control sidebar-control', leaflet_container);
	//var sidebar_button = L.DomUtil.create('div', 'sidebar-map-button-close', sidebar_div);
	/*sidebar_button.title = 'Показать/скрыть боковую панель';
	sidebar_button.className = 'sidebar-map-button-open';
	sidebar_button.onclick = function() {
		if (GIS.controls.sidebar.isVisible()) {
			GIS.controls.sidebar.hide();
			this.className = 'sidebar-map-button-close';
			console.log(this.style);
		} else {
			GIS.controls.sidebar.show();
			this.className = 'sidebar-map-button-open';
		}
	}*/

	L.control.zoom({
    	position:'topright'
	}).addTo(GIS.map);

	GIS.controls.locate = L.control.locate({
		position:'topright',
		strings: {
			title: 'Показать мое месторасположение',
			popup: 'Вы где-то здесь',
	    	outsideMapBoundsMsg: "You seem located outside the boundaries of the map"
		}
	}).addTo(GIS.map);

	GIS.controls.osmGeocoder = new L.Control.OSMGeocoder({
		collapsed: false,
	    position: 'topright',
	    text: 'Найти',
	    bounds: null,
	    email: null,
	    callback: function (results) {
	    		console.log(results);

	            var bbox = results[0].boundingbox,
	                first = new L.LatLng(bbox[0], bbox[2]),
	                second = new L.LatLng(bbox[1], bbox[3]),
	                bounds = new L.LatLngBounds([first, second]);
	            this._map.fitBounds(bounds);
	    }
	});
	GIS.controls.osmGeocoder.addTo(GIS.map);

 });
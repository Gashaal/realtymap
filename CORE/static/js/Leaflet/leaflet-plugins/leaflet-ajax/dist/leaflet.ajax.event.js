L.GeoJSON.AJAX.Event=L.GeoJSON.AJAX.extend({
	defaultEventParams:{
		featureLoad:  true,
		featureEvent: true,
		featureStyle: true,

		featureClass: 'gas',
		featureField: 'id',
		featureQuery: '',

		minZoom: 18,
		maxZoom: 19,
	},

	initialize: function (url, options) { // (String, Object)
		var eventParams = L.Util.extend({}, this.defaultEventParams);

		for (var i in options) {
			if (this.defaultEventParams.hasOwnProperty(i)) {
				eventParams[i] = options[i];
			}
		}

		this.eventParams = eventParams;
		this.eventParams.url = url;

		L.GeoJSON.AJAX.prototype.initialize.call(this, '', options);
	},

	onAdd: function (map) {
        var _this = this;

		_this._map = map;
		_this.eachLayer(map.addLayer, map);

		if (!_this.eventParams.featureEvent) {

			if (!_this.eventParams.featureStyle) {
				return _this;
			}

			_this._map.on('moveend', function(e){
				if (this.getZoom() <= _this.eventParams.maxZoom && this.getZoom() >= _this.eventParams.minZoom ) {
					_this.styleLayersExtent();
				}
			});

			return _this;
		}

		var pointsContains = [];

		_this._map.on('movestart', function(e){
			var bounds = this.getPixelBounds();
			pointsContains = [
				new L.Point(bounds.min.x, bounds.min.y),
				new L.Point(bounds.max.x, bounds.min.y),
				new L.Point(bounds.max.x, bounds.max.y),
				new L.Point(bounds.min.x, bounds.max.y),
			];
		});

		_this._map.on('moveend', function(e){
			if (this.getZoom() <= _this.eventParams.maxZoom && this.getZoom() >= _this.eventParams.minZoom ) {
				var pointsIntersects = L.PolyUtil.clipPolygon( pointsContains, this.getPixelBounds() );

				NBOX = '';
				if (pointsIntersects.length > 0) {
					for (var i in pointsIntersects) {
						pointsIntersects[i] = this.unproject(pointsIntersects[i]);
					}

					NBOX = new L.LatLngBounds(pointsIntersects).toBBoxString();
				}

				if (_this.eventParams.featureLoad) {
					_this.clearLayersExtent();
				}

				_this.styleLayersExtent();
				_this.addUrl(_this.eventParams.url + '?'
					+ 'FeatureClass=' + _this.eventParams.featureClass + '&'
					+ 'FeatureField=' + _this.eventParams.featureField + '&'
					+ 'FeatureQuery=' + _this.eventParams.featureQuery + '&'
					+ 'BBOX=' + this.getBounds().toBBoxString() + '&'
					+ 'NBOX=' + NBOX  + '&'
				);
			} else {
				_this.clearLayers();
			}
		});
    },

	clearLayersExtent: function() {
        if (this._map){
    		var extent = this._map.getBounds();

    		for (var key in this._layers) {
    			if (this._layers[key]['getBounds']) {
    				if ( ! extent.intersects( this._layers[key].getBounds() ) ) {
    					this.removeLayer(this._layers[key]);
    				}
    			} else if (this._layers[key]['getLatLng']) {
    				if ( ! extent.contains( this._layers[key].getLatLng() ) ) {
    					this.removeLayer(this._layers[key]);
    				}
    			}
    		}
        }
	},

	styleLayersExtent: function() {
		for (var key in this._layers) {
			this.resetStyle( this._layers[key] );
		}
	},

	_setLayerStyle: function (layer, style) {
        if (this._map){
    		if (typeof style === 'function') {

                    style = style(layer.feature, this._map.getZoom());

    		}

    		/*if (!layer.options.reset && layer.options.reset != undefined) {
    		return false;
    		}*/

    		if (layer.setStyle) {
    			layer.setStyle(style);
    		}
    		if (layer.setIcon) {
    			layer.setIcon(style);
    		}
        }
	},

	stamp: function (layer) {
		return this.eventParams.featureClass + '.' + layer.feature.properties.id;
	},

	addLayerID: function (layer) {
		var id = this.stamp(layer);

		this._layers[id] = layer;

		if (this._map) {
			this._map.addLayer(layer);
		}

		if (this.options.level && this.options.level != undefined){
			layer.setZIndexOffset(this.options.level);
		}

		return this;
	},

	addLayer: function (layer) {
		if (this._layers[this.stamp(layer)]) {
			return this;
		}

		layer.on(L.FeatureGroup.EVENTS, this._propagateEvent, this);

		this.addLayerID(layer);

		if (this._popupContent && layer.bindPopup) {
			layer.bindPopup(this._popupContent, this._popupOptions);
		}

		return this.fire('layeradd', {layer: layer});
	},

	removeLayerID: function (layer) {
		var id = this.stamp(layer);

		delete this._layers[id];

		if (this._map) {
			this._map.removeLayer(layer);
		}

		return this;
	},

	removeLayer: function (layer) {
		layer.off(L.FeatureGroup.EVENTS, this._propagateEvent, this);

		this.removeLayerID(layer);

		if (this._popupContent) {
			this.invoke('unbindPopup');
		}

		return this.fire('layerremove', {layer: layer});
	},
});

L.geoJson.ajax.event = function (geojson, options) {
	return new L.GeoJSON.AJAX.Event(geojson, options);
};


L.Marker.prototype._setPos = function (pos) {
	L.DomUtil.setPosition(this._icon, pos);

	if (this._shadow) {
		L.DomUtil.setPosition(this._shadow, pos);
	}

	this._zIndex = pos.y + this.options.zIndexOffset;

	this._resetZIndex();

	var rotation = this.options.icon.options.iconRotation;
	if (rotation) {
		this._icon.style.WebkitTransform = L.DomUtil.getTranslateString(pos) + ' rotate(' + rotation + 'deg)';
		this._icon.style.MozTransform = L.DomUtil.getTranslateString(pos) + ' rotate(' + rotation + 'deg)';
		this._icon.style.Transform = L.DomUtil.getTranslateString(pos) + ' rotate(' + rotation + 'deg)';
	}
    var opacity = this.options.icon.options.opacity;
    if (opacity) {
        this._icon.style.opacity = opacity;
    }
}

/*L.Marker.prototype.update = function () {
	if (this._icon) {
		var pos = this._map.latLngToLayerPoint(this._latlng).round();
		this._setPos(pos);

		var rotation = this.options.icon.options.iconRotation;
		if (rotation) {
			this._icon.style.WebkitTransform = L.DomUtil.getTranslateString(pos) + ' rotate(' + rotation + 'deg)';
			this._icon.style.MozTransform = L.DomUtil.getTranslateString(pos) + ' rotate(' + rotation + 'deg)';
			this._icon.style.Transform = L.DomUtil.getTranslateString(pos) + ' rotate(' + rotation + 'deg)';
		}
	}
	return this;
}*/
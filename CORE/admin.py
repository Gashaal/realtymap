from django.contrib import admin
import models

admin.site.register(models.flat)
admin.site.register(models.dacha)
admin.site.register(models.plot)
admin.site.register(models.office)
admin.site.register(models.trade)
admin.site.register(models.warehouse)
admin.site.register(models.production)
admin.site.register(models.garage)


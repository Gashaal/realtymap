# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os.path

from config import SVC_ENV, SVC_PATH, SVC_NAME


INSTALLED_APPS = []

INSTALLED_APPS.extend([
    SVC_NAME,
])

INSTALLED_APPS.extend([
    SVC_NAME + '.' + svc_env for svc_env in SVC_ENV['APPS']
])


STATICFILES_DIRS = []

STATICFILES_DIRS.extend([
    (SVC_NAME + '-' + svc_env, SVC_PATH + '/' + svc_env + '/static') for svc_env in SVC_ENV['APPS']
])

STATICFILES_DIRS.extend([
    (SVC_NAME, SVC_PATH + '/static'),
])


TEMPLATE_DIRS = []

TEMPLATE_DIRS.extend([
    SVC_PATH + '/' + svc_env + '/template' for svc_env in SVC_ENV['APPS']
])

TEMPLATE_DIRS.extend([
    SVC_PATH + '/template',
])
'use strict'; // Strict Mode

$( document ).ready(function() {

	GIS.layers.ZOOM = {
		25: { x: 1.0, y: 1.0 },
		24: { x: 1.0, y: 1.0 },
		23: { x: 1.0, y: 1.0 },
		22: { x: 1.0, y: 1.0 },
		21: { x: 1.0, y: 1.0 },
		20: { x: 1.0, y: 1.0 },
		19: { x: 1.0, y: 1.0 },
		18: { x: 1.0, y: 1.0 },
		17: { x: 1.0, y: 1.0 },
		16: { x: 0.9, y: 0.9 },
		15: { x: 0.9, y: 0.9 },
		14: { x: 0.8, y: 0.8 },
		13: { x: 0.8, y: 0.8 },
		12: { x: 0.7, y: 0.7 },
		11: { x: 0.7, y: 0.7 },
		10: { x: 0.5, y: 0.5 },
		 9: { x: 0.5, y: 0.5 },
		 8: { x: 0.5, y: 0.5 },
		 7: { x: 0.5, y: 0.5 },
		 6: { x: 0.5, y: 0.5 },
		 5: { x: 0.5, y: 0.5 },
		 4: { x: 0.5, y: 0.5 },
		 3: { x: 0.5, y: 0.5 },
		 2: { x: 0.5, y: 0.5 },
		 1: { x: 0.5, y: 0.5 },
		 0: { x: 0.5, y: 0.5 },
	};

	GIS.layers.getPopup = function(feature, layer) {
		var model = feature.model;
		var guid = feature.properties.guid;

		$.get(
			'/MAP/get_popup/',
			{model: model, guid: guid},
			function(data) {
				
				if (layer.getPopup()) {
					layer.bindPopup(data);
				} else {
					layer.bindPopup(data).openPopup();
				}
			}
		)
	}

	var Overlay = Backbone.Model.extend({
		defaults: {
		    "dataType":  "json",
		    "featureQuery": "",
		    "minZoom": 12,
		    "maxZoom": 18,
		    "url": "MAP/geojson/"
		},

		initialize: function() {
			var iconUrl = this.get('iconUrl');

			var layer = L.geoJson.ajax.event(this.get('url'), {
				dataType    : this.get('dataType'),
				featureAlias: this.get('featureAlias'),
				featureClass: this.get('featureClass'),
				featureModel: this.get('featureModel'),
				featureField: this.get('featureField'),
				featureQuery: this.get('featureQuery'),
				minZoom     : this.get('minZoom'),
				maxZoom     : this.get('maxZoom'),

				onEachFeature: function (feature, layer) {
					layer.feature.alias = this.featureAlias;
					layer.feature.class = this.featureClass;
					layer.feature.model = this.featureModel;
					
					layer.on('click', function() {
						GIS.layers.getPopup(feature, layer);
					})
				},
				
				style: function (feature, zoom) {
					var rezoom = GIS.layers.ZOOM;
					
					return L.icon({
						iconUrl : GIS.ENV.STATIC_URL + iconUrl,
						iconSize: [32 * rezoom[zoom].x, 37 * rezoom[zoom].y],
					});
				},
			}).addTo(GIS.map);

			this.set('layer', layer);
		},

		query: function(str_query) {
			var layer = this.get('layer');
			layer.options.featureQuery = str_query;
			this.refresh();
		},

		refresh: function() {
			this.get('layer').refresh();
		}

	})

	GIS.layers.FLAT = new Overlay({
		featureAlias: 'Квартиры', 
		featureClass: 'flat',
		featureModel: 'flat',
		featureField: 'id,guid,street',
		featureQuery: '',
		iconUrl: 'images/flat.png',
	});

	GIS.layers.FLAT.on('change:featureQuery', function(model) {
		model.query();
	});

	GIS.layers.DACHA = new Overlay({
		featureAlias: 'Дома и дачи', 
		featureClass: 'dacha', 
		featureModel: 'dacha',
		featureField: 'id,guid,street',
		featureQuery: '',
		iconUrl: 'images/home.png',
	});

	GIS.layers.DACHA.on('change:featureQuery', function(model) {
		model.query();
	});

	GIS.layers.PLOT = new Overlay({
		featureAlias: 'Дома и дачи', 
		featureClass: 'plot', 
		featureModel: 'plot',
		featureField: 'id,guid,street',
		featureQuery: '',
		iconUrl: 'images/plot.png',
	});

	GIS.layers.PLOT.on('change:featureQuery', function(model) {
		model.query();
	});

	GIS.layers.OFFICE = new Overlay({
		featureAlias: 'Офисы',
		featureClass: 'office',
		featureModel: 'office',
		featureField: 'id,guid,street',
		featureQuery: '',
		iconUrl: 'images/office-building.png',
	});

	GIS.layers.OFFICE.on('change:featureQuery', function(model) {
		model.query();
	});

	GIS.layers.TRADE = new Overlay({
		featureAlias: 'Офисы', 
		featureClass: 'trade', 
		featureModel: 'trade',
		featureField: 'id,guid,street',
		featureQuery: '',
		iconUrl: 'images/supermarket.png',
	});

	GIS.layers.TRADE.on('change:featureQuery', function(model) {
		model.query();
	});

	GIS.layers.WAREHOUSE = new Overlay({
		featureAlias: 'Офисы', 
		featureClass: 'warehouse', 
		featureModel: 'warehouse',
		featureField: 'id,guid,street',
		featureQuery: '',
		iconUrl: 'images/warehouse.png',
	});

	GIS.layers.WAREHOUSE.on('change:featureQuery', function(model) {
		model.query();
	});

	GIS.layers.PRODUCTION = new Overlay({
		featureAlias: 'Офисы',
		featureClass: 'production', 
		featureModel: 'production',
		featureField: 'id,guid,street',
		featureQuery: '',
		iconUrl: 'images/factory.png',
	});

	GIS.layers.PRODUCTION.on('change:featureQuery', function(model) {
		model.query();
	});

	GIS.layers.GARAGE = new Overlay({
		featureAlias: 'Офисы', 
		featureClass: 'garage', 
		featureModel: 'garage',
		featureField: 'id,guid,street',
		featureQuery: '',
		iconUrl: 'images/parkinggarage.png',
	});

	GIS.layers.GARAGE.on('change:featureQuery', function(model) {
		model.query();
	});

 });
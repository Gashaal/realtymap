# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import patterns, include, url

import views as app

urlpatterns = patterns('',
    url(r'^get_search_params/$', app.get_search_params),
)
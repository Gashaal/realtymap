# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import patterns, include, url

import views as app

urlpatterns = patterns('',
    url(r'^geojson/$', app.geojson),
    url(r'^get_popup/$', app.get_popup),
)

﻿#!/usr/local/bin/python
#
# -*- coding utf-8 -*-
from __future__ import unicode_literals
from django.contrib.gis.db import models
from uuid import uuid4

# Create your models here.

# CHOICES

OPERATION_TYPE_CHOICES = (
	(u'sale', u'Продажа'),
	(u'rent', u'Аренда')
)

FLAT_TYPE_CHOICES = (
	(u'room', u'Комната'),
	(u'dormitory', u'Гостинка'),
	(u'1room', u'1-комнатная квартира'),
	(u'2room', u'2-комнатная квартира'),
	(u'3room', u'3-комнатная квартира'),
	(u'4room', u'4-комнатная квартира'),
	(u'5room', u'5-комнатная квартира и более'),
)

MULTI_STOREY_BUILDING_CHOICES = (
	(u'panel', u'Панельный'),
	(u'brick', u'Кирпичный'),
	(u'monolithic', u'Монолитный'),
	(u'panel-brick', u'Панельно-кирпичный'),
	(u'monolithic-brick', u'Монолитно-кирпичный'),
)

CONDITION_BUILDING_CHOICES = (
	(u'new building', u'Новостройка'),
	(u'resellers', u'Вторичное жилье'),
	(u'modular', u'Шлакоблочный'),
	(u'stone',u'Каменный'),
)

DACHA_TYPE_CHOICES = (
	(u'1room', u'1 комната'),
	(u'2room', u'2 комнаты'),
	(u'3room', u'3 комнаты'),
	(u'4room', u'4 комнаты'),
	(u'5room', u'5 и более'),
)

HOUSING_MATERIAL_CHOICES = (
	(u'brick', u'Кирпичный'),
	(u'wood', u'Деревянный'),
	(u'block', u'Шлакоблочный'),
	(u'stone', u'Каменный'),
	(u'adobe', u'Саман'),
	(u'other', u'Другой'),
)

FLOT_TYPE_CHOICES = (
	(u'agriculture', u'Сельское хозяйство'),
	(u'housing', u'Индивидуальное жилищное строительство'),
	(u'com_construction', u'Коммерческое строительство'),
)

OFFICE_TYPE_CHOICES = (
	(u'business_center', u'Бизнес-центр'),
	(u'office_building', u'Офисное здание'),
	(u'bank', u'Банк'),
	(u'office_complex', u'Торгово-офисный комплекс'),
	(u'residential_complex', u'Офисно-жилой комплекс'),
	(u'capitol', u'Административное здание'),
	(u'entertainment_center', u'Торгово-развлекательный центр'),
	(u'warehouse_complex', u'Офисно-складской комплекс'),
)

TRADE_TYPE_CHOICES = (
	(u'shopping_mall', u'Торговый комплекс'),
	(u'entertainment_complex', u'Торгово-развлекательный комплекс'),
	(u'warehouse_complex', u'Торгово-складской комплекс'),
	(u'supermarket', u'Супермаркет'),
	(u'shop', 'Магазин'),
	(u'booth', u'Киоск'),
	(u'public_catering', u'Общепит'),
	(u'hotel', u'Гостиница'),
	(u'sauna', u'Сауна'),
	(u'auto_service', u'Автосервис/автомойка'),
	(u'fitness_club', u'Фитнес-клуб'),
	(u'nonresidential', u'Квартира в нежилом фонде'),
	(u'premisses', u'Помещение свободного назначения'),
)

WAREHOUSE_TYPE_CHOICES = (
	(u'warehouse_complex', u'Складской комплекс'),
	(u'office_complex', u'Офисно-складской комплекс'),
	(u'integrated_warehouse', u'Встроенное складское помещение'),
	(u'commerce_warehouse', u'Торгово-складской комплекс'),
	(u'production_warehouse', u'Производственно-складской комплекс'),
	(u'logistics_park', u'Логистический парк')
)

PRODUCTION_TYPE_CHOICES = (
	(u'manufacturing_complex', u'Производственный комплекс'),
	(u'ready production', u'Готовое производство'),
	(u'production_warehouse', u'Производственно-складской комплекс')
)

GARAGE_TYPE_CHOICES = (
	(u'major_garage', u'Капитальный гараж'),
	(u'parking_place', u'Машиноместо на улице'),
	(u'underground_parking', u'Машиноместо на подземной парковке')
)

class common_table(models.Model):
	guid = models.CharField(max_length=38, null=False, blank=False, default=str(uuid4()).upper())
	operation_type = models.CharField(max_length=20, null=False, blank=False, choices=OPERATION_TYPE_CHOICES)
	date = models.DateTimeField(auto_now=True, auto_now_add=True, verbose_name=u'Дата размещения')
	street = models.CharField(max_length=100, null=False, blank=False, verbose_name=u'Улица')
	house = models.IntegerField(null=False, blank=False, verbose_name=u'Номер дома')
	fraction = models.CharField(max_length=10, null=True, blank=True, verbose_name=u'Доп. номер дома')
	price = models.FloatField(null=False, verbose_name=u'Цена')
	photo = models.BooleanField(default=False, verbose_name=u'Фото')
	video = models.BooleanField(default=False, verbose_name=u'Видео')
	quickly = models.BooleanField(default=False, verbose_name=u'Срочно')
	exclusive = models.BooleanField(default=False, verbose_name=u'Эксклюзив')
	description  = models.CharField(max_length=255, null=True, blank=True, verbose_name=u'Описание')
	geom = models.PointField(srid=4326)
	objects = models.GeoManager()

	class Meta:
		abstract = True


class flat(common_table):
	type_object = models.CharField(max_length=20, null=False, blank=False, choices=FLAT_TYPE_CHOICES, verbose_name=u'Тип объекта')
	type_building = models.CharField(max_length=30, null=False, blank=False, choices=MULTI_STOREY_BUILDING_CHOICES, verbose_name=u'Тип дома')
	condition_bulding = models.CharField(max_length=30, null=False, blank=False, choices=MULTI_STOREY_BUILDING_CHOICES, verbose_name=u'Состояние дома')
	balcony = models.BooleanField(default=False, verbose_name=u'Балкон')
	phone = models.BooleanField(default=False, verbose_name=u'Телефон')
	internet = models.BooleanField(default=False, verbose_name=u'Интернет')
	total_area = models.FloatField(verbose_name=u'Общая площадь')
	living_area = models.FloatField(verbose_name=u'Жилая площадь')
	kitchen_area = models.FloatField(verbose_name=u'Площадь кухни')
	floors = models.IntegerField(verbose_name=u'Этажность дома')
	floor_flat = models.IntegerField(verbose_name=u'Этаж помещения')

	class Meta:
		db_table = u'flat'
		verbose_name = u'Квартиры и комнаты'
		
class dacha(common_table):
	type_object = models.CharField(max_length=20, null=False, blank=False, choices=DACHA_TYPE_CHOICES, verbose_name=u'Тип объекта')
	type_building = models.CharField(max_length=30, null=False, blank=False, choices=HOUSING_MATERIAL_CHOICES, verbose_name=u'Тип дома')
	total_area = models.FloatField(verbose_name=u'Общая площадь')
	living_area = models.FloatField(verbose_name=u'Жилая площадь')
	kitchen_area = models.FloatField(verbose_name=u'Площадь кухни')
	plot_area = models.FloatField(verbose_name=u'Площадь участка')
	part_house = models.BooleanField(default=False, verbose_name=u'Часть дома')

	class Meta:
		db_table = u'dacha'
		verbose_name = u'Дома и дачи'


class plot(common_table):
	type_object = models.CharField(max_length=20, null=False, blank=False, choices=FLOT_TYPE_CHOICES, verbose_name=u'Тип объекта')
	plot_area = models.FloatField(verbose_name=u'Площадь участка')

	class Meta:
		db_table = u'plot'
		verbose_name = u'Участки'


class office(common_table):
	type_object = models.CharField(max_length=20, null=False, blank=False, choices=OFFICE_TYPE_CHOICES, verbose_name=u'Тип объекта')
	room_area = models.FloatField(verbose_name=u'Площадь помещения')
	plot_area = models.FloatField(verbose_name=u'Площадь участка')
	business = models.BooleanField(default=False, verbose_name=u'Готовый бизнес')

	class Meta:
		db_table = u'office'
		verbose_name = u'Офисы'

class trade(common_table):
	type_object = models.CharField(max_length=20, null=False, blank=False, choices=TRADE_TYPE_CHOICES, verbose_name=u'Тип объекта')
	room_area = models.FloatField(verbose_name=u'Площадь помещения')
	plot_area = models.FloatField(verbose_name=u'Площадь участка')
	business = models.BooleanField(default=False, verbose_name=u'Готовый бизнес')
	
	class Meta:
		db_table = u'trade'
		verbose_name = u'Торговля'

class warehouse(common_table):
	type_object = models.CharField(max_length=20, null=False, blank=False, choices=WAREHOUSE_TYPE_CHOICES, verbose_name=u'Тип объекта')
	room_area = models.FloatField(verbose_name=u'Площадь помещения')
	plot_area = models.FloatField(verbose_name=u'Площадь участка')
	business = models.BooleanField(default=False, verbose_name=u'Готовый бизнес')

	class Meta:
		db_table = u'warehouse'
		verbose_name = u'Склады'

class production(common_table):
	type_object = models.CharField(max_length=20, null=False, blank=False, choices=PRODUCTION_TYPE_CHOICES, verbose_name=u'Тип объекта')
	room_area = models.FloatField(verbose_name=u'Площадь помещения')
	plot_area = models.FloatField(verbose_name=u'Площадь участка')
	business = models.BooleanField(default=False, verbose_name=u'Готовый бизнес')

	class Meta:
		db_table = u'production'
		verbose_name = u'Производство'

class garage(common_table):
	type_object = models.CharField(max_length=20, null=False, blank=False, choices=GARAGE_TYPE_CHOICES, verbose_name=u'Тип объекта')
	area = models.FloatField(verbose_name=u'Площадь помещения')

	class Meta:
		db_table = u'garage'
		verbose_name = u'Парковка'


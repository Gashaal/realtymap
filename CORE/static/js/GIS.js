﻿'use strict'; // Strict Mode

( function () {
	window.GIS = {
		map: null,
		layers: {},
		controls: {},
		
		CONFIG: {},
		ENV: {},
	};
})();